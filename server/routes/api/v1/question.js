(() => {

	let path = __filename.split("routes")[1].split(".")[0];

	// Extracts the file name which makes the final endpoint, 
	// if you want a custom path then mention the final end point 
	// eg: /api/v1/test 

	let handler = (request, h) => {
		let payload = JSON.parse(JSON.stringify(request.payload))
		console.log(payload);
		return "api v1 question";

	}

	let route = {
		method : "POST",
		path,
		handler
	}

	module.exports = route;
})();
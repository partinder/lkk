(() => {
	const fs = require("fs");
	let routesToExport = fs.readdirSync(__dirname)
	.filter(file => file != 'index.js')
	.map(fileName => require("./"+fileName));

	module.exports = routesToExport.flat(10)
})()
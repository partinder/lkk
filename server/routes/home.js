(() => {
    const Path = require("path");

    console.log(Path.join(__dirname,"../../client/dist"))

    let handler = {
        directory: {
            path: Path.join(__dirname,"../../client/dist"),
            listing: true,
            index: true
        }

    }

    let route = {
        path: "/{param*}",
        method: "GET",
        handler
    }

    module.exports = route

})();
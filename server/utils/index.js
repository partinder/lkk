(() => {
	const fs = require("fs");

	let utilsToExport = fs.readdirSync(__dirname)
	.filter(file => file != 'index.js')
	.reduce((acc,curr) => {
		acc[curr.split(".")[0]] = require("./"+curr)
		return acc;
	},{});

	module.exports = {...utilsToExport};
})()
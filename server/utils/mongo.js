(() => {

    let MongoClient = require("mongodb").MongoClient;
    let { mongo : config } = require("../config.js");
    let url = config.host + ":"+ config.port;

    let db;

    let connect = () => {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, (err, client) => {
                if (err){
                	console.log(err)
                	process.exit(1);
                }
                db = client.db(config.dbName)
                console.log(`DB Server started at ${url}`)
                return resolve()
            })
        })
    }

    module.exports = {
        connect,
        getDb : () => db
    }

})();
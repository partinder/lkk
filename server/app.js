(() => {

    const Hapi = require('@hapi/hapi');
    const Routes = require("./routes");
    const Mongo = require("./utils").mongo;
    const {server : config} = require("./config.js")

    const plugins = {
        inert: require("@hapi/inert")
    }

    const server = Hapi.server({
        port: config.port,
        host: config.host
    })


    let initServer = async () => {

        await server.register(Object.keys(plugins).map(key => plugins[key]));
        server.route(Routes);
        await Mongo.connect();


        await server
        .start()
        .then(() => {
        	console.log(`Web Server started at ${server.info.uri}`);
        })
        .catch(err => {
        	console.log("Server could not Start:")
        	console.log(err);
        })

        process.on('uncaughtException', err => {
            console.log(err);
            process.exit(1);
        });

    }

    initServer();

})();
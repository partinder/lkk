(() => {
	module.exports = {
		server : {
			host : "localhost",
			port : 3000
		},
		mongo : {
			host : "mongodb://localhost",
			port : 27017,
			dbName : "lkk"
		}
	}
})()
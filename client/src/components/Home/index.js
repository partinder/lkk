import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "jquery/dist/jquery.min.js";

import "./style.css";

import React from "react";

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="vertical-center">
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-4 mx-2">
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                placeholder="Type in question.."
                name="email"
                required
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
(() => {
    const Path = require("path");
    const HtmlWebpackPlugin = require("html-webpack-plugin");
    const MiniCssExtractPlugin = require("mini-css-extract-plugin");


    let env = process.env.NODE_ENV || "development";


    module.exports = {
        mode: env,
        entry: Path.join(__dirname, "/client/src/app.js"),
        output: {
            filename: "bundle.js",
            path: Path.join(__dirname, "/client/dist/"),
            clean: true
        },
        devServer: {
            contentBase: Path.join(__dirname, "/client/dist/"),
            compress: true,
            port: 9000,
            hot: true
        },
        module: {
            rules: [{
                test: /\.?js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react']
                    }
                }

            }, {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    env == "development" ? "style-loader" : MiniCssExtractPlugin.loader,"css-loader"
                    // style loader isfaster, hence use in dev, 
                    //in prod create seperate files for CSS extraction in sepearte files                
                ]
            }, {
                test: /\.svg$/,
                use: 'file-loader'
            }]
        },
        plugins: [
            new HtmlWebpackPlugin({ template: Path.join(__dirname, "/client/src/", "index.html") }), // inject JS into the index.html file
            new MiniCssExtractPlugin({ filename: '[name].css' }) // Creates sepearte CSS files its used
        ],
        optimization: {
            minimize: true
        }
    }

})()